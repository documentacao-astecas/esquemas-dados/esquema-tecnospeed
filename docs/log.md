# Log

Tabela responsável por armazenar os extratos recebidos das contas bancárias.

## Descrição dos campos da tabela

  | Campo        | Tipo             | Descrição                                                                  |
  | :----------- | :--------------- | :------------------------------------------------------------------------- |
  | `id`         | **serial**       | Indentificador da tabela (Primary Key).                                    |
  | `pagador_id` | **int8**         | Foreing Key da tabela [pagador](index.md).                                 |
  | `evento`     | **varchar(255)** | Recebe o evento da API de pagamento.                                       |
  | `response`   | **jsonb**        | Armazena as informações de resposta.                                       |
  | `endpoint`   | **varchar(255)** | Rota de resposta da requisição.                                            |
  | `ip`         | **varchar(100)** | IP recebido durante a resposta da requisição.                              |
  | `created_at` | **timestamp**    |                                                                            |
  | `updated_at` | **timestamp**    |                                                                            |

## Relacionamentos

  | Tabela                    | Tipo            | Descrição                                                                     |
  | :------------------------ | :-------------- | :---------------------------------------------------------------------------- |
  | [^^`pagador`^^](index.md) | **Foreing Key** | As contas bancárias do pagador gerarão informaçõe que serão aqui armazenadas. |