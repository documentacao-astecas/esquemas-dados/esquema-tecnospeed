# Pagador

Tabela responsável por armazenar os dados do dono da conta cujo extrato será extraído.

## Descrição dos campos da tabela

  | Campo                | Tipo             | Descrição                                                                   |
  | :------------------- | :--------------- | :-------------------------------------------------------------------------- |
  | `id`                 | **int4**         | Indentificador da tabela (Primary Key).                                     |
  | `name`               | **varchar(255)** | Nome do pagador.                                                            |
  | `cpfcnpj`            | **varchar(14)**  | CPF / CNPJ do pagador.                                                      |
  | `accounts`           | **jsonb**        | Dados das contas que o pagador possui e que serão fontes de extrato.        |
  | `street`             | **varchar(255)** | Rua onde o pagador mora.                                                    |
  | `neighborhood`       | **varchar(255)** | Bairro onde o pagador mora.                                                 |
  | `addressnumber`      | **varchar(10)**  | Número de onde o pagador mora.                                              |
  | `adddresscomplement` | **varchar(255)** | Complemento de onde o pagador mora.                                         |
  | `city`               | **varchar(255)** | Cidade onde o pagador mora.                                                 |
  | `zipcode`            | **varchar(10)**  | Código postal do pagador.                                                   |
  | `token`              | **varchar(255)** | Identificador do pagador.                                                   |
  | `empresa_id`         | **int8**         | Foreing Key da tabela [empresas](#) (esquema astecas).                      |
  | `unidade_id`         | **int8**         | Foreing Key da tabela [unidades](#) (esquema astecas).                      |
  | `created_at`         | **timestamp**    |                                                                             |
  | `updated_at`         | **timestamp**    |                                                                             |

*[CPF]: Cadastro de pessoas físicas,
*[CNPJ] : Cadastro de pessoas jurídicas

## Relacionamentos

  | Tabela                      | Tipo            | Descrição                                                                                   |
  | :-------------------------- | :-------------- | :------------------------------------------------------------------------------------------ |
  | [^^`astecas.empresas`^^](#) | **Foreing Key** | As contas bancárias de um pagador estão ligadas a uma empresa.                              |
  | [^^`astecas.unidades`^^](#) | **Foreing Key** | As contas bancárias de um pagador estão ligadas a uma unidade.                              |
  | [^^`log`^^](log.md)         | **Primary Key** | As contas bancárias do pagador gerarão informações que serão armazenadas na tabela de logs. |